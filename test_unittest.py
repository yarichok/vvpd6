import unittest
from vvpd5.vvpd4 import check_color_is_gray, color_to_gray


class MainTestArea(unittest.TestCase):
    def setUp(self):
        print('Тест начат')

    def tearDown(self):
        print('Тест закончен')

    def test_first_function_good(self):
        self.assertEqual(check_color_is_gray('#FFFFFF'), True)

    def test_first_function_good_1(self):
        self.assertEqual(check_color_is_gray('#000000'), True)

    def test_first_function_good_2(self):
        self.assertEqual(check_color_is_gray('#F37AD1'), False)

    def test_second_function_good(self):
        self.assertEqual(color_to_gray('#07FE67'), ('F7', '00', '97'))

    def test_first_function_bad(self):
        self.assertRaises(TypeError, check_color_is_gray('ADCFE2'))
