"""
Смолянинов Ярослав Евгеньевич, КИ21-17/2Б.
Программа проверяет серый ли цвет, если нет говорит, сколько нужно добавить RGB к исходному цвету
"""


def check_color_is_gray(line: str) -> bool:
    """
    Проверяет серый ли цвет был передан.
    :type line: str
    :param line: Цвет, который передали
    :return: True, если цвет серый, и False в ином случае
    """
    new_line = line[1:]
    if new_line[:2] == new_line[2:4] == new_line[4:]:
        return True
    else:
        return False


def color_to_gray(color: str) -> tuple:
    """
    Выводит, сколько нужно добавить красного, зеленого и синего цвета чтобы получить серый.
    :param color: Цвет, который передали
    :return: Кортеж со значениями, чтобы получить серый
    """
    new_color = color[1:]
    list = []
    alphabet = '0123456789ABCDEF'
    for i in new_color:
        list.append(alphabet.find(i))
    maximum = max((list[0] + list[1]), (list[2] + list[3]), (list[4] + list[5]))
    maximum_1 = max(list[0], list[2], list[4])
    counter = 0
    second_list = []
    for i in range(0, 3):
        line = str()
        if list[counter] + list[counter+1] == maximum:
            second_list.append('00')
        else:
            argument = (maximum_1 - list[counter])
            if new_color[counter+1] == '0':
                line += (alphabet[argument] + alphabet[(maximum - maximum_1)])
                second_list.append(line)
            else:
                if (maximum - maximum_1) < list[counter+1]:
                    argument -= 1
                    line += (alphabet[argument] + alphabet[16 - list[counter+1] + (maximum - maximum_1)])
                    second_list.append(line)
                else:
                    line += (alphabet[argument] + alphabet[(maximum - maximum_1)-list[counter+1]])
                    second_list.append(line)
        counter += 2
    return tuple(second_list)


# row = input('Введите свой цвет:')
# flag = check_color_is_gray(row)
# while flag != True:
#     new_row = color_to_gray(row)
#     print(new_row)
#     break
# else:
#     print('Ваш цвет', row, 'серый.')