import pytest
from vvpd5.vvpd4 import check_color_is_gray
from vvpd5.vvpd4 import color_to_gray


@pytest.fixture(autouse=True)
def start_tests():
    print('Тест начат')


def test_first_function_good():
    assert check_color_is_gray('#F0F0F0') is True


def test_first_function_good_1():
    assert check_color_is_gray('#123456') is False


@pytest.mark.parametrize('line, result', [('#FFFFFF', True),
                                          ('#FCBA8C', False),
                                          ('#574012', False)])
def test_first_function_param(line, result):
    assert check_color_is_gray(line) == result


@pytest.mark.parametrize('color, output', [('#574012', ('00', '17', '45')),
                                           ('#AC57FD', ('51', 'A6', '00')),
                                           ('#07FE67', ('F7', '00', '97'))])
def test_second_function_param(color, output):
    assert color_to_gray(color) == output


@pytest.mark.parametrize('param, exception', [('ADCFE2', IndexError),
                                              ('#DC', IndexError)])
def test_first_function_error(param, exception):
    with pytest.raises(exception):
        color_to_gray(param)
