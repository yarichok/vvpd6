# Мой первый опыт работы с GitLab.
Markdown — это облегчённый язык разметки, созданный с целью обозначения форматирования в простом тексте, с максимальным сохранением его читаемости человеком, и пригодный для машинного преобразования в языки для продвинутых публикаций. С помощью него можно писать файл [**readme**](https://ru.wikipedia.org/wiki/README-%D1%84%D0%B0%D0%B9%D0%BB "Ссылка на википедию") для проектов.

В этом проекте хранятся файлы с практических работ №4 и №5. Мы добавили их на GitLab из локального репозитория. Ниже показан [docstring](https://en.wikipedia.org/wiki/Docstring#:~:text=In%20programming%2C%20a%20docstring%20is,a%20specific%20segment%20of%20code.&text=This%20allows%20the%20programmer%20to,help%20system%2C%20or%20as%20metadata. "Ссылка на википедию") для функций из практической работы:

**Docstring первой функции:**
```python
def check_color_is_gray(line: str) -> bool:
    """
    Проверяет серый ли цвет был передан.
    :type line: str
    :param line: Цвет, который передали
    :return: True, если цвет серый, и False в ином случае
    """
```
**Docstring второй функции:**
```python
def color_to_gray(color: str) -> tuple:
    """
    Выводит, сколько нужно добавить красного, зеленого и синего цвета чтобы получить серый.
    :param color: Цвет, который передали
    :return: Кортеж со значениями, чтобы получить серый
    """
```
Код практической работы №4 можно посмотреть отдельно в файле **vvdp4.py**.
Ниже прикреплен скриншот код написанных тестов для практической работы №4:
 
![Код тестов](https://sun9-57.userapi.com/impg/7uP4PNlg3BlONvEBAeHtbzLTARHLhWIF7AUMbA/mPdPtaNWSeg.jpg?size=433x524&quality=96&sign=2126885cef3514c1cf00743a27f792df&type=album)

